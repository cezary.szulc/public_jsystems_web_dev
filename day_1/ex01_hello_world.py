#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask

app = Flask(__name__)


# hello world
@app.route('/')
def hello():
    return 'Hello world'

#######################################################
# renderuje tekst
#
# Do pokazania minimalna aplikacja flaskowa

# Najpierw zaimportowaliśmy klasę Flask.
# Instancją tej klasy będzie nasza aplikacja WSGI.
# Następnie tworzymy instancję tej klasy.
#
# Pierwszy argument to nazwa modułu aplikacji lub pakietu.
# Jeśli używasz pojedynczego modułu (jak w tym przykładzie),
# powinieneś użyć __name__. W zależności od tego,
# czy skrypt został uruchomiony jako aplikacja, czy importowany jako moduł,
# nazwa będzie inna („__main__” w porównaniu z nazwą importu).
# Jest to potrzebne, aby Flask wiedział, gdzie szukać szablonów,
# plików statycznych i tak dalej.
#
# /application.py
# /templates
#     /hello.html
# Case 2: a package:
#
# /application
#     /__init__.py
#     /templates
#         /hello.html
#
# Następnie używamy dekoratora route (), aby powiedzieć Flaskowi,
# jaki URL powinien wywołać naszą funkcję.
#
# Funkcja zwraca wiadomość,
# którą chcemy wyświetlić w przeglądarce użytkownika.
#
# Możliwośc uruchomienia są dwa.
# Pierwsze przez  uruchomienie skryptu kończącego się app.run()
# Drugie przez
# $ export FLASK_APP=ex01_hello_world.py
# $ flask run
# Różnice między sposobami odpalenia
# Flask jest dobry do uruchomienia lokalnego serwera programistycznego,
# ale po każdej zmianie kodu musiałbyś go zrestartować ręcznie. To nie jest
# zbyt miłe i Flask może zrobić to lepiej. Jeśli włączysz obsługę debugowania,
# serwer przeładuje się po zmianach w kodzie, a także zapewni Ci pomocny
# debuger, jeśli coś pójdzie nie tak. Aby włączyć wszystkie funkcje
# programistyczne (w tym tryb debugowania), możesz wyeksportować zmienną
# środowiskową FLASK_ENV i ustawić ją na development
# przed uruchomieniem serwera:
# $ export FLASK_ENV = development
#
# Nowoczesne aplikacje internetowe wykorzystują znaczące adresy URL,
# aby pomóc użytkownikom. Użytkownicy chętniej polubią stronę i wrócą,
# jeśli strona będzie zawierała znaczący adres URL, który mogą zapamiętać
# i wykorzystać do bezpośredniego odwiedzenia strony.
# Użyj dekoratora route (), aby powiązać funkcję z adresem URL.
#
# Najpopularniejsze błędy przy uruchamianiu aplikacji
# Literówka lub fakt, że nie utworzono obiektu aplikacji.
#######################################################


# if __name__ == '__main__':
#     app.run(port=5000, debug=True)
